const AWS = require('aws-sdk');

AWS.config.loadFromPath('./aws.json');
AWS.config.apiVersions = {
    comprehend: '2017-11-27'
};


const comprehend = new AWS.Comprehend();



const getSentiment = (text, lang = 'es') => {
    var params = {
        LanguageCode: 'es',
        TextList: [text]
    };
    return new Promise((resolve, reject) => {
        comprehend.batchDetectSentiment(params, (err, data) => {
            if (err) {
                reject(err.stack)
            } else {
                const result = data.ResultList && data.ResultList.length > 0 ? data.ResultList[0].Sentiment : "NONE";
                resolve({
                    text,
                    sentiment: result
                })
            }
        });
    });
}


module.exports = {
    getSentiment
}