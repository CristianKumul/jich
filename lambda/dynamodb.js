const AWS = require('aws-sdk'),
    table = 'jich';
    AWS.config.loadFromPath('./aws.json');
AWS.config.update({
    region: 'us-east-1'
});

const create_UUID = () =>{
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c==='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}
const ddb = new AWS.DynamoDB({
    apiVersion: '2012-10-08'
});
/**
 * Return Item value by key/id
 * @param {string} key Unique key from table
 */
const getItem = (key) => {
    const params = {
        TableName: table,
        Key: {
            'id': {
                S: key
            },
        }
    };
    return new Promise((resolve, reject) => {
        ddb.getItem(params, function (err, data) {
            if (err) {
                console.log("Error", err);
                reject(err);
            } else {
                console.log("Success", data.Item);
                resolve(data.Item);
            }
        });
    });
}
/**
 * 
 * @param {string} key value of unique indentifier
 * @param {string} stringItem value of item, must be a string also for an object
 */
const saveItem = (sessionId, question, answer,interview, sentiment = 'NONE') => {
    const id = create_UUID();
    const params = {
        TableName: table,
        Item: {
            'id': {
                S: id
            },
            'session': {
                S: sessionId
            },
            'interview': {
                S: interview
            },
            'question': {
                S: question
            },
            'answer': {
                S: answer
            },
            'sentiment': {
                S: sentiment
            },
        }
    };
    return new Promise((resolve, reject) => {
        ddb.putItem(params, function (err, data) {
            if (err) {
                console.log("Error", err);
                reject(err);
            } else {
                console.log("Success", data);
                resolve(data);
            }
        });
    })
}
/**
 * remove an item from table
 * @param {string} key unique id
 */
const deleteItem = (key) => {
    const params = {
        TableName: table,
        Key: {
            'id': {
                S: key
            },
        }
    };
    return new Promise((resolve, reject) => {
        ddb.deleteItem(params, function (err, data) {
            if (err) {
                console.log("Error", err);
                reject(err);
            } else {
                console.log("Success", data);
                resolve(data);
            }
        });
    })
}

const saveList = (list) => {
    const promiselist = [];
    for (var i = list.length; i--;) {
        const item = list[i];
        const p = saveItem(item.id + "", JSON.stringify(item));
        promiselist.push(p);
    }
    Promise.all(promiselist);
}

module.exports = {
    getItem,
    saveItem,
    deleteItem,
    saveList
}