// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');
const comprehend = require('./comprehend');
const dynamo = require('./dynamo');

const {
  getRequestType,
  getIntentName,
  getSlotValue,
  getDialogState,
} = require('ask-sdk-core');

const networkQuestions = ["¿Qué significa Wifi?",
"¿Cuáles son las capas del modelo de referencia OSI?",
"¿Cuál es la herramienta numero uno para el diagnostico de redes?",
"¿Cuál es la unidad de datos de protocolo de la capa de red del modelo de referencia OSI?"];

const developerQuestions = ["¿Qué es la programación orientada a objetos?",
"¿Qué es el encapsulamiento?",
"¿Qué es el polimorfismo?",
"¿Qué es una clase?",
"¿Qué es un diagrama UML?"];

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speechText = `Bienvenido al simulador de entrevistas, 
        puedes iniciar con la palabra prueba seguido de redes, desarrollo o base de datos`;
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .withSimpleCard('Bienvenido a JICH','simulador de entrevistas')
            .getResponse();
    }
};
const HelloWorldIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'HelloWorldIntent';
    },
    handle(handlerInput) {
        const speechText = 'Hello World!';
        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

//----------------------------------------------------------------------------------
// Answer flow for responses
const answerInterviewHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'answerInterviewIntent';
    },
     handle(handlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        const { interviewType } = sessionAttributes;
        
        if(interviewType === 'developer'){
            return handleDeveloper(handlerInput);
        }
        else
        if(interviewType === 'network'){
            return handleNetwork(handlerInput);
        }
    }
};

//----------------------------------------------------------------------------------
//function for testing only
async function handleDeveloper(handlerInput){
    //get the attributes
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const { counter,interviewType } = sessionAttributes;
    let question = '';
    let speechText = '';
    
    if(sessionAttributes.counter < developerQuestions.length - 1){
        question = getNextQuestion(handlerInput);
    }
    else{
        return handlerInput.responseBuilder
        .speak('Gracias por participar en la entrevista')
        .getResponse();
    }
    
    speechText = question;
    
    //get the slots for user response
    const req = handlerInput.requestEnvelope.request;
    const filledSlots = req.intent.slots;
    console.log("los slots " + JSON.stringify(filledSlots));
    
    const currentSlot =  filledSlots && filledSlots.answer ? filledSlots.answer.value : '';
    console.log(currentSlot);
    
    if(typeof currentSlot !== 'undefined'){
     const sentiment = await comprehend.getSentiment(currentSlot);
     console.log(sentiment.sentiment);
     let dyna = await dynamo.saveItem(sessionAttributes.sessionID,question,currentSlot,'developer',sentiment.sentiment);
     console.log(dyna);
    }
    
    return handlerInput.responseBuilder
        .speak(speechText)
        .reprompt(speechText)
        .withSimpleCard(question,'Pregunta abierta, puedes responder iniciando por -mi respuesta es: - ')
        .getResponse();
}

const InterviewDevelopHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'InterviewDevelopIntent';
    },
    async handle(handlerInput) {
        
    //get the attributes
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const { counter,interviewType } = sessionAttributes;
    
    if(typeof interviewType === 'undefined'){
        sessionAttributes.interviewType = 'developer';
        handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
    }
    
    let question = getNextQuestion(handlerInput);
    let speechText = '';
    
    if(typeof counter === 'undefined')
        speechText = 'Esta es la prueba de desarrollo, empezemos con la primer pregunta, ' + question ;
    else
        speechText = question;
        
    return handlerInput.responseBuilder
        .speak(speechText)
        .reprompt(speechText)
        .withSimpleCard(question,'Pregunta abierta, puedes responder iniciando por -mi respuesta es: - ')
        .getResponse();
    }
};

//------------------------------------------------------------------------------------------
// Function for testing only
async function handleNetwork(handlerInput){
    //get the attributes
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const { counter } = sessionAttributes;
    const { interviewType } = sessionAttributes;
    
    let question = '';
    let speechText = '';
    
    if(sessionAttributes.counter < networkQuestions.length - 1){
        question = getNextQuestion(handlerInput);
    }
    else{
        return handlerInput.responseBuilder
        .speak('Gracias por participar en la entrevista')
        .getResponse();
    }
    
    speechText = question;
    
    //get the slots for user response
    const req = handlerInput.requestEnvelope.request;
    const filledSlots = req.intent.slots;
    console.log("los slots " + JSON.stringify(filledSlots));
    
    const currentSlot =  filledSlots && filledSlots.answer ? filledSlots.answer.value : '';
    console.log(currentSlot);
    
    if(typeof currentSlot !== 'undefined'){
     const sentiment = await comprehend.getSentiment(currentSlot);
     console.log(sentiment.sentiment);
     let dyna = await dynamo.saveItem(sessionAttributes.sessionID,question,currentSlot,'network',sentiment.sentiment);
     console.log(dyna);
    }
    
    return handlerInput.responseBuilder
        .speak(speechText)
        .reprompt(speechText)
        .withSimpleCard(question,'Pregunta abierta, puedes responder iniciando por -mi respuesta es: - ')
        .getResponse();
}

const InterviewNetworkHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'InterviewNetworkIntent';
    },
    async handle(handlerInput) {
        
    //get the attributes
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const { counter } = sessionAttributes;
    const { interviewType } = sessionAttributes;
    
    if(typeof interviewType === 'undefined')
    {
        sessionAttributes.interviewType = 'network';
        handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
    }
    
    let question = getNextQuestion(handlerInput);
    let speechText = '';
    
    if(typeof counter === 'undefined')
        speechText = 'Esta es la prueba de redes, empezemos con la primer pregunta, ' + question ;
    else
        speechText = question;
    
    return handlerInput.responseBuilder
        .speak(speechText)
        .reprompt(speechText)
        .withSimpleCard(question,'Pregunta abierta, puedes responder iniciando por -mi respuesta es: - ')
        .getResponse();
    }
};

function getNextQuestion(handlerInput,questionType){
    //fist time sessionAttributes no data
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const { counter , interviewType } = sessionAttributes;

    if(typeof counter === 'undefined'){
        sessionAttributes.counter = 0;
        sessionAttributes.sessionID = create_UUID();
    }
    else
    {
        sessionAttributes.counter = sessionAttributes.counter + 1;
    }
    

    handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
    
    let dataQuestion = '';
    
    if(interviewType === 'network'){
        dataQuestion = networkQuestions[sessionAttributes.counter];   
    }
    else
    if(interviewType === 'developer'){
        dataQuestion = developerQuestions[sessionAttributes.counter];
    }
    
    return dataQuestion;
}

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speechText = 'You can say hello to me! How can I help?';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .withSimpleCard(question,'Pregunta abierta, puedes responder iniciando por -mi respuesta es: - ')
            .getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speechText = 'Goodbye!';
        return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = handlerInput.requestEnvelope.request.intent.name;
        const speechText = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.message}`);
        const speechText = `Sorry, I couldn't understand what you said. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};

const RequestLog = {
    async process(handlerInput) {
    //const attributes = handlerInput.attributesManager.getRequestAttributes();
    //console.log("Atributos de alexa: " + JSON.stringify(attributes));
    const slotValue = getSlotValue(handlerInput.requestEnvelope, 'bases');
    console.log('ok');
    //console.log("REQUEST ENVELOPE = " + JSON.stringify(handlerInput.requestEnvelope));
    
    return;
    }
};

const create_UUID = () =>{
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c==='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

// This handler acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        //HelloWorldIntentHandler,
        InterviewNetworkHandler,
        InterviewDevelopHandler,
        answerInterviewHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler) // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    .addErrorHandlers(
        ErrorHandler)
    //.addRequestInterceptors(RequestLog)
    .lambda();
